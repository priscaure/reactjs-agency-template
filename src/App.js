import React, {Component} from 'react'
import styles from './App.module.scss'
import settings from './settings.json'
import {Route, Switch, withRouter} from 'react-router-dom'
import Home from './screens/home/Home';
import AgencyToolbar from './components/agencyToolbar/AgencyToolbar';
import Projects from './screens/projects/Projects';
import Why from './screens/why/Why';
import Contact from './screens/contact/Contact';
import AgencyFooter from './components/agencyFooter/AgencyFooter';


class App extends Component {
  state = {
    menuIsOpen: false
};

onDrawerClick = (isOpen) => {
    this.setState({menuIsOpen: isOpen})
};

render() {
  return (
    <div className={[styles.portfolio, this.state.menuIsOpen && styles.menuIsOpen].join(' ')}>
        <div>
            <AgencyToolbar
                name={settings.name}
                content={settings.static.toolbar}
                onDrawerClick={(isOpen) => this.onDrawerClick(isOpen)}/>
        </div>
        <div className={styles.content}>
            <Switch>
                <Route exact path="/" render={() => <Home content={settings.static.home}/>}/>
                <Route path="/projects" render={() => <Projects content={settings.static.projects}/>}/>
                <Route path="/why-us" render={() => <Why content={settings.static.why}/>}/>
                <Route path="/contact" render={() => <Contact content={settings.static.contact}/>}/>
            </Switch>
        </div>
        <AgencyFooter content={settings.static.footer}/>
      </div>
    )
  }
}

export default withRouter(App);
