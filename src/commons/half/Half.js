import React from 'react'
import PropTypes from 'prop-types'
import styles from './Half.module.scss'

const Half = (props) => {
    const {id, halfClassName, left, right, leftClassName, rightClassName, reverse, leftFlex, rightFlex, reverseWrapperClassName, reverseClassName, asColumn, asColumnClassName} = props;

    const asColumnClasses = [styles.asColumn, asColumnClassName].join(' ');

    return (
        <div id={id} className={[styles.half, halfClassName, reverse && styles.reverse, reverse && reverseWrapperClassName, asColumn && asColumnClasses].join(' ')}>
            <div
                className={[styles.left, leftClassName, reverse && reverseClassName].join(' ')}
                style={leftFlex && {flex: leftFlex}}>
                {left}
            </div>
            <div
                className={[styles.right, rightClassName].join(' ')}
                style={rightFlex && {flex: rightFlex}}
            >
                {right}
            </div>
        </div>
    );
};

Half.propTypes = {
    left: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
    right: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
    reverse: PropTypes.bool,
    leftFlex: PropTypes.number,
    rightFlex: PropTypes.number
};

export default Half