import React from "react";
import PropTypes from 'prop-types'

const Facebook = ({color}) => {
    return (
        <svg width={27} height={27}>
            <path
                d="M24.5 1h-22A1.5 1.5 0 0 0 1 2.5v22A1.5 1.5 0 0 0 2.5 26H14v-8h-3v-4h3v-3a5 5 0 0 1 5-5h3v4h-2a2 2 0 0 0-2 2v2h4l-.5 4H18v8h6.5a1.5 1.5 0 0 0 1.5-1.5v-22A1.5 1.5 0 0 0 24.5 1z"
                stroke={color}
                fill="transparent"
            />
        </svg>
    )
};

Facebook.propTypes = {
    color: PropTypes.string.isRequired
};

export default Facebook;
