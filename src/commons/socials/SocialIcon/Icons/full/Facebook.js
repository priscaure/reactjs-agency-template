import React from "react";
import PropTypes from 'prop-types'

const Facebook = ({fill}) => {
    return (
        <svg width={26} height={26}>
            <path
                d="M24 0H2C.897 0 0 .897 0 2v22c0 1.103.897 2 2 2h10.5a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.5-.5h-2a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 .5-.5h2a.5.5 0 0 0 .5-.5v-2C13 7.467 15.467 5 18.5 5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-2c-.827 0-1.5.673-1.5 1.5v1a.5.5 0 0 0 .5.5h3a.501.501 0 0 1 .496.562l-.5 4A.5.5 0 0 1 21 18h-2.5a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .5.5H24c1.103 0 2-.897 2-2V2c0-1.103-.897-2-2-2"
                fill={fill}
                fillRule="evenodd"
            />
        </svg>
    )
};

Facebook.propTypes = {
    fill: PropTypes.string.isRequired
};

export default Facebook;
