import React from "react";
import PropTypes from 'prop-types'

const Linkedin = ({fill}) => {
    return (
        <svg width={26} height={26}>
            <path
                d="M23 22.5a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5v-7c0-.827-.673-1.5-1.5-1.5s-1.5.673-1.5 1.5v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5v-12a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v.519A4.925 4.925 0 0 1 18 10c2.757 0 5 2.243 5 5v7.5zM6.5 9A2.503 2.503 0 0 1 4 6.5C4 5.122 5.122 4 6.5 4S9 5.122 9 6.5 7.878 9 6.5 9zM9 22.5a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5v-12a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v12zM24 0H2C.897 0 0 .897 0 2v22c0 1.103.897 2 2 2h22c1.103 0 2-.897 2-2V2c0-1.103-.897-2-2-2z"
                fill={fill}
                fillRule="evenodd"
            />
        </svg>
    )
};

Linkedin.propTypes = {
    fill: PropTypes.string.isRequired
};

export default Linkedin;
