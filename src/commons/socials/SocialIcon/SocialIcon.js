import React from "react";
import PropTypes from 'prop-types'
import styles from './SocialIcons.module.scss'
import FacebookFull from "./Icons/full/Facebook";
import LinkedinFull from "./Icons/full/Linkedin";
import TwitterFull from "./Icons/full/Twitter";
import FacebookBorder from "./Icons/border/Facebook";
import TwitterBorder from "./Icons/border/Twitter";

const SocialIcon = ({fill, format, link, network, className}) => {
    const socialIconClasses = [styles.socialIcon, className];
    if (format === "border") {
        socialIconClasses.push(styles.border)
    }
    let icon = <FacebookFull fill={fill}/>;
    switch (network) {
        case 'twitter':
            if (format === "full")
                icon = <TwitterFull fill={fill}/>;
            else
                icon = <TwitterBorder color={fill}/>;
            break;
        case 'linkedin':
            icon = <LinkedinFull fill={fill}/>;
            break;
        default:
            if (format === "full")
                icon = <FacebookFull fill={fill}/>;
            else
                icon = <FacebookBorder color={fill}/>;
            break;
    }
    return (
        <a
            className={socialIconClasses.join(' ')}
            href={link}
            target="_blank"
            rel="noopener noreferrer">
            {icon}
        </a>
    )
};

SocialIcon.propTypes = {
    fill: PropTypes.string.isRequired,
    format: PropTypes.oneOf(["border", "full"]),
    link: PropTypes.string.isRequired,
    network: PropTypes.string.isRequired,
    className: PropTypes.string
};

SocialIcon.defaultPros = {
    format: "full"
};

export default SocialIcon;
