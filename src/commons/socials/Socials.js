import React from 'react'
import PropTypes from 'prop-types'
import styles from './Socials.module.scss'
import SocialIcon from "./SocialIcon/SocialIcon";

const Socials = ({fill, format, networks, className}) => {
    const socialsClasses = [styles.socials, className].join(' ');
    const content = networks.map(item => {
        return (
            <SocialIcon
                key={item.network}
                format={format}
                fill={fill}
                link={item.link}
                network={item.network}/>
        )
    });
    return (
        <div className={socialsClasses}>
            {content}
        </div>
    );
};

Socials.propTypes = {
    fill: PropTypes.string.isRequired,
    className: PropTypes.string,
    networks: PropTypes.arrayOf(PropTypes.shape({
        link: PropTypes.string,
        network: PropTypes.string
    })).isRequired,
    format: PropTypes.oneOf(["border", "full"])
};

Socials.defaultProps = {
    networks: [],
    className: ""
};

export default Socials