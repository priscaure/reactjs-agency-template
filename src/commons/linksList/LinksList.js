import React from 'react'
import {NavLink} from "react-router-dom";

const links = (menu, linkClassName, linkActiveClassName) => {
    return menu.map(item => {
        return (
            <li key={item.title}>
                <NavLink
                    to={item.link}
                    exact={item.exact}
                    className={[linkClassName, item.className].join(' ')}
                    activeClassName={linkActiveClassName}>
                    {item.title}
                </NavLink>
            </li>
        )
    })
};


const LinksList = ({menu, linkClassName, linkActiveClassName}) => (
    <ul>
        {
            links(menu, linkClassName, linkActiveClassName)
        }
    </ul>
);

export default LinksList