import React from 'react'
import styles from './Input.module.scss'

const Input = (props) => {
    let inputElement = null;
    const inputClasses = [styles.inputElement];

    const required = props.shouldValidate.required;

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(styles.invalid);
        inputClasses.push(props.invalidClassName)
    }

    switch (props.elementType) {
        case('input'):
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}/>;
            break;
        case ('textarea'):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}/>;
            break;
        case ('select'):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.changed}>
                    {props.elementConfig.options.map(option => (
                        <option
                            key={option.value}
                            value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>);
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}/>
    }

    return (
        <div className={[styles.input, props.inputClassName].join(' ')}>
            {
                props.label !== "" &&
                <label className={styles.label}>{props.label}{required && "*"}</label>
            }
            {inputElement}
        </div>
    )
};

export default Input