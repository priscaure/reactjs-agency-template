import React from 'react'
import PropTypes from 'prop-types'
import styles from './Footer.module.scss'
import Column from './column/Column';

const Footer = (props) => {
    const {columns, copyright, footerClass, copyrightClass, columnsClass, columnClass, columnTitleClass, columnContentClass, wrapperClass, otherContent} = props;
    const footerClasses = [styles.footer, footerClass].join(' ');
    const copyrightClasses = [styles.copyright, copyrightClass].join(' ');
    const columnsClasses = [styles.columns, columnsClass].join(' ');
    const wrapperClasses = [styles.wrapper, wrapperClass].join(' ');

    const content = columns.map((item, index) => {
        return (
            <Column
                key={index}
                title={item.title}
                flex={item.flex}
                columnClass={columnClass}
                titleClass={columnTitleClass}
                contentClass={columnContentClass}>
                {item.content}
            </Column>
        )
    });

    return (
        <div className={footerClasses}>
            <div className={wrapperClasses}>
                {otherContent}
                <div className={columnsClasses}>
                    {content}
                </div>
            </div>
            {
                copyright &&
                <div className={copyrightClasses} dangerouslySetInnerHTML={{__html: copyright}}/>
            }
        </div>
    )
};

Footer.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        flex: PropTypes.number,
        content: PropTypes.element
    })).isRequired,
    copyright: PropTypes.string,
    footerClass: PropTypes.string,
    copyrightClass: PropTypes.string,
    columnsClass: PropTypes.string,
    columnClass: PropTypes.string,
    columnTitleClass: PropTypes.string,
    columnContentClass: PropTypes.string
};

Footer.defaultProps = {
    columns: [],
    footerClass: "",
    copyrightClass: "",
    columnsClass: "",
    columnClass: "",
    columnTitleClass: "",
    columnContentClass: "",
    copyright: "",
    otherContent: ""
};

export default Footer