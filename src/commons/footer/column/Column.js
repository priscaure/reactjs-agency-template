import React from 'react'
import PropTypes from 'prop-types'
import styles from './Column.module.scss'

const Column = (props) => {
    const {children, title, flex, columnClass, titleClass, contentClass} = props;
    const columnClasses = [styles.column, columnClass].join(' ');
    const titleClasses = [styles.title, titleClass].join(' ');
    const contentClasses = [styles.content, contentClass].join(' ');
    return (
        <div className={columnClasses} style={flex && {flex}}>
            {title && (
                <h4 className={titleClasses}>{title}</h4>
            )}
            <div className={contentClasses}>
                {children}
            </div>
        </div>
    )
};

Column.propTypes = {
    title: PropTypes.string,
    columnClass: PropTypes.string,
    titleClass: PropTypes.string,
    contentClass: PropTypes.string,
};

Column.defaultProps = {
    columnClass: "",
    titleClass: "",
    contentClass: ""
};

export default Column