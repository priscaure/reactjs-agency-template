import React from 'react'
import PropTypes from 'prop-types'
import styles from './DrawerToggle.module.scss'

const drawerToggle = (props) => (
    <div
        onClick={props.clicked}
        className={[styles.drawerToggle, props.className].join(' ')}>
        {props.open ?
            <div className={[styles.close, props.closeClassName].join(' ')}/>
            :
            <div className={[styles.open, props.openClassName].join(' ')}>
                <div/>
                <div/>
                <div/>
            </div>
        }
    </div>
);

drawerToggle.propTypes = {
    clicked: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};

export default drawerToggle