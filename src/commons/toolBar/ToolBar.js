import React from 'react'
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom'
import styles from './ToolBar.module.scss'
import DrawerToggle from "../drawerToggle/DrawerToggle";

class ToolBar extends React.Component {

    state = {
        drawerOpened: this.props.menuIsOpen
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.menuIsOpen !== nextProps.menuIsOpen) {
            this.setState({drawerOpened: nextProps.menuIsOpen})
        }
    }

    drawerToggleClicked = () => {
        this.setState(previousState => {
            return {
                drawerOpened: !previousState.drawerOpened
            }
        }, () => {
            this.props.onDrawerClick(this.state.drawerOpened)
        })
    };

    links = (menu, linkClassName, linkActiveClassName) => {
        return menu.map(item => {
            return (
                <li key={item.title} className={item.submenu && styles.hasChild}>
                    <NavLink
                        to={item.link}
                        exact={item.exact}
                        className={[styles.link, linkClassName, item.className].join(' ')}
                        onClick={() => {
                            if (this.state.drawerOpened) {
                                this.drawerToggleClicked()
                            }
                        }}
                        activeClassName={linkActiveClassName}>
                        {item.title}
                    </NavLink>
                    {
                        item.submenu &&
                        <ul className={styles.submenu}>{this.links(item.submenu, linkClassName, linkActiveClassName)}</ul>
                    }
                </li>
            )
        })
    };

    render() {
        const {toolbarClassName, menuClassName, drawerIcon, logoIcon, menuPosition, menu, otherContent, otherContentAreLinks, otherContentClassName, linkClassName, linkActiveClassName, responsiveClassName, logoPosition, drawerToggleClassName, drawerOpenClassName, drawerCloseClassName} = this.props;
        const toolbarClasses = [styles.toolbar, toolbarClassName];

        const links = this.links(menu, linkClassName, linkActiveClassName);

        const menuContent = drawerIcon ? drawerIcon : <ul>{links}</ul>;

        const menuElement = (
            <div className={[styles.menu, menuClassName].join(' ')}>
                {menuContent}
                <DrawerToggle
                    className={drawerToggleClassName}
                    open={this.state.drawerOpened}
                    openClassName={drawerOpenClassName}
                    closeClassName={drawerCloseClassName}
                    clicked={() => this.drawerToggleClicked()}/>
                <div
                    className={[styles.responsive, this.state.drawerOpened && styles.open, responsiveClassName].join(' ')}>
                    {otherContent}
                    {menuContent}
                </div>
            </div>
        );


        const logoElement = (
            <div className={styles.logo}>
                {logoIcon}
            </div>
        );

        let otherContentClass = styles.menu;
        if (!otherContentAreLinks)
            otherContentClass = "";

        const otherElement = (
            <div className={[otherContentClass, otherContentClassName].join(' ')}>
                {otherContent}
            </div>
        );


        let leftElement = logoElement;
        let centerElement = otherElement;
        let rightElement = menuElement;

        if (menuPosition === "left") {
            // add reverse to component
            toolbarClasses.push(styles.reverse)
        } else if (menuPosition === "center") {
            centerElement = menuElement;
            rightElement = otherElement;
        }

        if (logoPosition === "top") {
            toolbarClasses.push(styles.logoTop)
        } else if (logoPosition === "center") {
            leftElement = menuElement;
            centerElement = logoElement;
            rightElement = otherElement;
            if (menuPosition === "right") {
                rightElement = menuElement;
                leftElement = otherElement;
            }
        }

        return (
            <header className={toolbarClasses.join(" ")}>
                {leftElement}
                {centerElement}
                {rightElement}
            </header>
        )
    }
};

ToolBar.propTypes = {
    onLogoClick: PropTypes.func,
    menuPosition: PropTypes.oneOf(['left', 'right', 'center']),
    logoPosition: PropTypes.oneOf(['left', 'top', 'center']),
    toolbarClassName: PropTypes.string,
    menu: PropTypes.arrayOf(PropTypes.shape({
        link: PropTypes.string,
        title: PropTypes.string,
        className: PropTypes.string
    })),
    otherContent: PropTypes.element
};

ToolBar.defaultProps = {
    menuPosition: "right",
    logoPosition: "left",
    toolbarClassName: "",
    menuIsOpen: false,
    otherContentAreLinks: true,
    menu: [],
    onDrawerClick: () => {
    }
};

export default ToolBar