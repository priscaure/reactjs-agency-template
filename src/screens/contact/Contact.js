import React, {Component} from 'react'
import styles from './Contact.module.scss'
import Half from '../../commons/half/Half';
import WrapperContent from '../../components/wrapperContent/WrapperContent';
import {checkValidity} from '../../utils/checkValidity';
import AgencyInput from '../../components/agencyInput/AgencyInput';

class Contact extends Component {

    state = {
        form: this.props.content.form.controls,
        formIsValid: false,
    };

    componentDidMount() {
        window.scrollTo(0,0)
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.form
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid
        }
        this.setState({form: updatedForm, formIsValid})
    };

    contactWays() {
        return (
            <React.Fragment>
                <h2>{this.props.content.ways.name}</h2>
                <div className={styles.contactWays}
                     dangerouslySetInnerHTML={{__html: this.props.content.ways.content}}/>
            </React.Fragment>
        )
    }

    contactHandler = (event) => {
        event.preventDefault();

        const formData = {};
        for (let formElementIdentifier in this.state.form) {
            formData[formElementIdentifier] = this.state.form[formElementIdentifier].value;
        }
        console.log("The next message will be sent: ", formData)
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.form) {
            formElementsArray.push({
                id: key,
                config: this.state.form[key]
            })
        }
        let form = (
            <React.Fragment>
                <h2>{this.props.content.form.title}</h2>
                <form onSubmit={this.contactHandler}>
                    {formElementsArray.map(formElement => (
                        <AgencyInput
                            key={formElement.id}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            label={formElement.config.label}
                            invalid={!formElement.config.valid}
                            shouldValidate={formElement.config.validation}
                            touched={formElement.config.touched}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))}
                    <div className={styles.requiredText}>{this.props.content.form.requiredText}</div>
                    <div className={styles.input}>
                        <button disabled={!this.state.formIsValid}>{this.props.content.form.buttonValue}</button>
                    </div>
                </form>
            </React.Fragment>
        );
        return (
            <div className={styles.contact}>
                <WrapperContent
                    wrapperClass={styles.wrapper}
                    wrapperContentClass={styles.wrapperContent}
                    antennaClass={styles.antenna}>
                    <Half
                        halfClassName={styles.half}
                        left={form}
                        leftClassName={styles.left}
                        right={this.contactWays()}
                        rightClassName={styles.right}/>
                </WrapperContent>
            </div>
        )
    }
}

export default Contact