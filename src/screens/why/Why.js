import React, {Component} from 'react'
import styles from './Why.module.scss'
import {Fade} from 'react-reveal';
import PageHeader from '../../components/pageHeader/PageHeader';
import WrapperContent from '../../components/wrapperContent/WrapperContent';
import Gallery from '../../components/gallery/Gallery';
import Slider from '../../components/slider/Slider';
import apiAgency from '../../api/agency';
import {wpTeam, wpWhyUs} from '../../api/wpWrapper';
import ProgressBar from '../../utils/ProgressBar/ProgressBar';

class Why extends Component {

    state = {
        content: null
    };

    whyData = null;
    teamData = null;

    constructor() {
        super();
        ProgressBar.init()
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        if (this.props.content.apiAccess !== null) {
            apiAgency.get(`pages?slug=${this.props.content.apiAccess}`)
                .then(async response => {
                    this.whyData = await wpWhyUs(response.data[0]);
                    this.onAPIFinished()
                });
            apiAgency.get('posts?_envelop&_embed&categories=5')
                .then(team => {
                    this.teamData = wpTeam(team.data);
                    this.onAPIFinished()
                })
        } else {
            this.setState({header: this.props.content.header})
        }
    }

    onAPIFinished() {
        if (this.whyData !== null
            && this.teamData !== null) {
            this.setState({
                content: {
                    ...this.whyData,
                    team: {
                        ...this.whyData.team,
                        slides: [
                            ...this.teamData
                        ]
                    }
                }
            })
        }
    }

    render() {
        if (this.state.content === null) {
            ProgressBar.start();
            return <div/>
        }
        ProgressBar.stop();

        return (
            <Fade big>
                <div className={styles.why}>
                    <PageHeader
                        halfClassName={styles.half}
                        leftContent={<div
                            className={styles.pageHeaderLeft}
                            dangerouslySetInnerHTML={{__html: this.state.content.header.left}}/>
                        }
                        rightContent={<div dangerouslySetInnerHTML={{__html: this.state.content.header.right}}/>}
                        asColumn/>
                    <WrapperContent
                        title={this.state.content.team.title}
                        wrapperClass={styles.wrapper}
                        wrapperContentClass={styles.wrapperContent}>
                        <Slider slides={this.state.content.team.slides}/>
                    </WrapperContent>
                    <Gallery content={this.state.content.gallery}/>
                </div>
            </Fade>
        )
    }
}

export default Why