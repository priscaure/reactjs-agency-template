import React, {Component} from 'react'
import styles from './Home.module.scss'
import {Fade} from 'react-reveal';
import PageHeader from '../../components/pageHeader/PageHeader';
import WrapperContent from '../../components/wrapperContent/WrapperContent';
import LinkSeeMore from '../../components/linkSeeMore/LinkSeeMore';
import ProjectsListLight from '../../components/projectsListLight/ProjectsListLight';
import PartnersList from '../../components/partnersList/PartnersList';
import ProgressBar from '../../utils/ProgressBar/ProgressBar';
import apiAgency from '../../api/agency';
import {wpHome, wpPartners, wpProjects} from "../../api/wpWrapper";

class Home extends Component {
    state = {
        content: null
    };
    pageContent = null;
    partnersData = null;
    highlightData = null;

    constructor() {
        super();
        ProgressBar.init()
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        if (this.props.content.apiAccess !== null) {
            apiAgency.get(`pages?slug=${this.props.content.apiAccess}`)
                .then(response => {
                    this.pageContent = wpHome(response.data[0]);
                    apiAgency.get(`/posts?envelop&_embed&include=${response.data[0].acf.partners}`)
                        .then(partners => {
                            this.partnersData = {
                                partners: {
                                    list: wpPartners(partners.data)
                                }
                            };
                            this.onAPIFinished()
                        });

                    apiAgency.get(`/posts?_envelop&_embed&categories=4`)
                        .then(highlight => {
                            this.highlightData = {
                                projects: {
                                    list: wpProjects(highlight.data)
                                }
                            };
                            this.onAPIFinished()
                        })
                })
        } else {
            this.setState({content: this.props.content})
        }
    }

    onAPIFinished = () => {
        if (this.pageContent !== null
            && this.partnersData !== null
            && this.highlightData !== null) {
            const page = {
                ...this.pageContent,
                ...this.partnersData,
                ...this.highlightData
            };
            this.setState({content: page})
        }
    };

    render() {
        const {projects, partners} = this.props.content;

        if (this.state.content === null) {
            ProgressBar.start();
            return <div/>
        }
        ProgressBar.stop();
        return (
            <Fade big>
                <div className={styles.home}>
                    <PageHeader
                        halfClassName={styles.half}
                        leftContent={<div dangerouslySetInnerHTML={{__html: this.state.content.header.left}}/>}
                        rightContent={
                            <div className={styles.pageHeaderRight}>
                                <div dangerouslySetInnerHTML={{__html: this.state.content.header.right}}/>
                                {
                                    (this.state.content.header.linkUrl && this.state.content.header.linkText) &&
                                    <LinkSeeMore to={this.state.content.header.linkUrl}
                                                 text={this.state.content.header.linkText}/>
                                }
                            </div>
                        }/>
                    <div className={styles.wrapperIntroduction}>
                        <div className={styles.introduction}/>
                        <div className={styles.quote}>
                            {this.state.content.quote.content}
                            <LinkSeeMore to={this.state.content.quote.linkUrl}
                                         text={this.state.content.quote.linkText}/>
                        </div>
                    </div>
                    <WrapperContent
                        title={projects.title}
                        wrapperClass={styles.wrapperRecentProjects}
                        wrapperContentClass={styles.wrapperContentRecentProjects}>
                        <ProjectsListLight data={this.state.content.projects.list}/>
                    </WrapperContent>
                    <WrapperContent
                        title={partners.title}
                        wrapperClass={styles.wrapperPartners}
                        wrapperContentClass={styles.wrapperContentPartners}>
                        <PartnersList data={this.state.content.partners.list}/>
                    </WrapperContent>
                </div>
            </Fade>
        )
    }
}

export default Home