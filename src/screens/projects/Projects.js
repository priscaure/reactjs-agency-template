import React, {Component} from 'react'
import styles from './Projects.module.scss'
import {Fade} from 'react-reveal';
import PageHeader from '../../components/pageHeader/PageHeader';
import WrapperContent from '../../components/wrapperContent/WrapperContent';
import ProjectsList from '../../components/projectsList/ProjectsList';
import apiAgency from '../../api/agency';
import {wpPage, wpProjects} from '../../api/wpWrapper';
import ProgressBar from '../../utils/ProgressBar/ProgressBar';

class Projects extends Component {

    state = {
        content: null
    };

    headerDatAgency
    projectsData = null;

    constructor() {
        super();
        ProgressBar.init()
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        if (this.props.content.apiAccess !== null && this.props.content.apiAccess !== undefined) {
            apiAgency.get(`pages?slug=${this.props.content.apiAccess}`)
                .then(response => {
                    this.headerData = {
                        header: wpPage(response.data[0])
                    };
                    this.onAPIFinished()
                });
            apiAgency.get(`/posts?_envelop&_embed&categories=2`)
                .then(projects => {
                    this.projectsData = {
                        projects: {
                            list: wpProjects(projects.data)
                        }
                    };
                    this.onAPIFinished()
                })
        } else {
            this.setState({content: this.props.content})
        }
    }

    onAPIFinished() {
        if (this.headerData !== null
            && this.projectsData !== null) {
            this.setState({
                content: {
                    ...this.headerData,
                    ...this.projectsData
                }
            })
        }
    }

    render() {
        const {projects} = this.props.content;
        if (this.state.content === null) {
            ProgressBar.start();
            return <div/>
        }
        ProgressBar.stop();
        return (
            <Fade big>
                <div className={styles.projects}>
                    <PageHeader
                        halfClassName={styles.half}
                        leftContent={<div dangerouslySetInnerHTML={{__html: this.state.content.header.left}}/>}
                        rightContent={<div dangerouslySetInnerHTML={{__html: this.state.content.header.right}}/>}/>
                    <WrapperContent
                        title={projects.title}
                        wrapperClass={styles.wrapper}
                        wrapperContentClass={styles.wrapperContent}>
                        <ProjectsList data={this.state.content.projects.list}/>
                        {/*<LinkSeeMore to="#see-more" text={projects.linkText}/>*/}
                    </WrapperContent>
                </div>
            </Fade>
        )
    }
}

export default Projects