import React from 'react'
import styles from './AgencyInput.module.scss'
import Input from "../../commons/input/Input";

const AgencyInput = (props) => (
    <Input
        elementType={props.elementType}
        elementConfig={props.elementConfig}
        value={props.value}
        label={props.label}
        invalid={props.invalid}
        shouldValidate={props.shouldValidate}
        touched={props.touched}
        changed={props.changed}
        inputClassName={styles.input}
        invalidClassName={styles.invalid}/>
);

export default AgencyInput