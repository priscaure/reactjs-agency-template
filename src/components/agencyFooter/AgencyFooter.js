import React from 'react'
import styles from './AgencyFooter.module.scss'
import Footer from "../../commons/footer/Footer";
import AgencyLinksList from "../agencyLinksList/AgencyLinksList";
import AgencySocials from "../agencySocials/AgencySocials";

const AgencyFooter = (props) => {
    const columns = [];
    for (let i = 0; i < props.content.columns.length; i++) {
        const item = props.content.columns[i];
        const column = {
            flex: item.flex,
            title: item.title
        };

        if (item.content) column.content = <div dangerouslySetInnerHTML={{__html: item.content}}/>;
        if (item.links) column.content = <AgencyLinksList links={item.links}/>;
        if (item.networks) column.content = <AgencySocials networks={item.networks} fill="#BAC3C7"/>;

        columns.push(column)
    }

    return (
        <Footer
            footerClass={styles.footer}
            wrapperClass={styles.wrapper}
            columns={columns}
            columnsClass={styles.columns}
            columnClass={styles.column}
            columnTitleClass={styles.title}
            columnContentClass={styles.content}
            copyright={props.content.copyright}
            copyrightClass={styles.copyright}/>
    )
};

export default AgencyFooter
