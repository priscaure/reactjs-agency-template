import React from 'react'
import styles from './PageHeader.module.scss'
import Half from "../../commons/half/Half";

const PageHeader = (props) => {
    const {leftContent, rightContent, halfClassName, asColumn} = props;
    return (
        <Half
            halfClassName={[styles.half, halfClassName].join(' ')}
            left={leftContent}
            leftClassName={styles.left}
            right={rightContent}
            rightClassName={styles.right}
            asColumn={asColumn}
            asColumnClassName={styles.asColumn}/>
    )
};

PageHeader.defaultProps = {
    row: false
};

export default PageHeader