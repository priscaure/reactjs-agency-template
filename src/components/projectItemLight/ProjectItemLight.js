import React from 'react'
import styles from './ProjectItemLight.module.scss'
import LinkSeeMore from "../linkSeeMore/LinkSeeMore";

const ProjectItemLight = (props) => (
    <div className={styles.projectItemLight}>
        <img src={props.image} alt=""/>
        <h3 className={styles.title}>{props.title}</h3>
        <div className={styles.date}>{props.date}</div>
        <LinkSeeMore to={props.link} text="En savoir plus"/>
    </div>
);

export default ProjectItemLight