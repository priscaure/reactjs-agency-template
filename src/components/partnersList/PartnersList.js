import React from 'react'
import styles from './PartnersList.module.scss'

const PartnersList = ({data}) => (
    <div className={styles.partnersList}>
        {
            data.map((item, index) => {
                return <a key={index} href={item.link}><img src={item.image} alt=""/></a>
            })
        }
    </div>
);

export default PartnersList