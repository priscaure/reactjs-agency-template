import React from 'react'
import styles from './ProjectsListLight.module.scss'
import ProjectItemLight from "../projectItemLight/ProjectItemLight";

const ProjectsListLight = ({data, projectItemLightClassName}) => (
    <div className={styles.projectsListLight}>
        {
            data.map((item, index) => {
                return <ProjectItemLight
                    id={item.id}
                    key={index}
                    title={item.title}
                    date={item.date}
                    image={item.image}
                    link={item.link}
                    className={projectItemLightClassName}
                />
            })
        }
    </div>
);

export default ProjectsListLight