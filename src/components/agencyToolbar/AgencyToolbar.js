import React from 'react'
import styles from './AgencyToolbar.module.scss';
import Toolbar from "../../commons/toolBar/ToolBar";
import {NavLink} from "react-router-dom";

const AgencyToolbar = (props) => (
    <Toolbar
        logoIcon={(
            <div className={styles.logo}>
                <NavLink to="/">{props.name}</NavLink>
            </div>)}
        menuPosition={props.content.positionMenu}
        logoPosition={props.content.positionLogo}
        menu={props.content.menu}
        toolbarClassName={styles.toolbar}
        menuClassName={styles.menu}
        linkClassName={styles.link}
        linkActiveClassName={styles.linkActive}
        responsiveClassName={styles.responsive}
        onDrawerClick={props.onDrawerClick}
    />
);

export default AgencyToolbar