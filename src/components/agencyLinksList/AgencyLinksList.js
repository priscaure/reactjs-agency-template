import React from 'react'
import styles from './AgencyLinksList.module.scss'
import LinksList from "../../commons/linksList/LinksList";

const AgencyLinksList = (props) => (
    <LinksList
        menu={props.links}
        linkClassName={styles.link}
        linkActiveClassName={styles.linkActive}
    />
);

export default AgencyLinksList