import React from 'react'
import './Slider.css'
import SliderSlick from "react-slick";

const Slider = (props) => {
    const settings = {
        className: ["slider", props.slides.length <= 2 && "slider-custom-width", props.slides.length <= 5 && "slider-without-padding-bottom"].join(' '),
        centerMode: true,
        centerPadding: "30px",
        infinite: true,
        dots: true,
        arrows: false,
        slidesToShow: props.slides.length < 5 ? props.slides.length : (window.innerWidth > 955 ? 5 : 3),
        slidesToScroll: 1,
        focusOnSelect: true,
        swipe: window.innerWidth <= 955,
        speed: 500
    };
    return (
        <SliderSlick {...settings}>
            {
                props.slides.map((item, index) => {
                    return (
                        <div className={"slider-item"} key={index}>
                            <img src={item.image} alt=""/>
                            <div className="informations">
                                <div className="name">{item.name}</div>
                                <div className="job" dangerouslySetInnerHTML={{__html: item.job}}/>
                            </div>
                        </div>
                    )
                })
            }
        </SliderSlick>
    )
};

export default Slider