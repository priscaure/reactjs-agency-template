import React from 'react'
import styles from './Gallery.module.scss';
import PhotoGallery from 'react-photo-gallery';
import Lightbox from 'react-images';

class Gallery extends React.Component {
    constructor() {
        super();
        this.state = {currentImage: 0};
        this.closeLightbox = this.closeLightbox.bind(this);
        this.openLightbox = this.openLightbox.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
    }

    openLightbox(event, obj) {
        this.setState({
            currentImage: obj.index,
            lightboxIsOpen: true,
        });
    }

    closeLightbox() {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }

    gotoPrevious() {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }

    gotoNext() {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }

    render() {
        return (
            <div className={styles.gallery}>
                <h3>{this.props.content.title}</h3>
                <PhotoGallery margin={10} direction={"column"} photos={this.props.content.images}
                              onClick={this.openLightbox}/>
                <Lightbox images={this.props.content.images}
                          onClose={this.closeLightbox}
                          onClickPrev={this.gotoPrevious}
                          onClickNext={this.gotoNext}
                          currentImage={this.state.currentImage}
                          isOpen={this.state.lightboxIsOpen}
                />
            </div>
        )
    }
};

export default Gallery