import React from 'react'
import styles from './LinkSeeMore.module.scss'
import {HashLink as Link} from "react-router-hash-link";

const LinkSeeMore = ({to, text}) => (
    <Link smooth className={styles.linkSeeMore} to={to}>{text}</Link>
);

export default LinkSeeMore