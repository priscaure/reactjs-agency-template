import React from 'react'
import styles from './WrapperContent.module.scss'

const WrapperContent = ({children, title, wrapperClass, wrapperContentClass, antennaClass}) => (
    <div className={[styles.wrapper, wrapperClass].join(' ')}>
        <div className={[styles.wrapperContent, wrapperContentClass].join(' ')}>
            <div className={[styles.antenna, antennaClass].join(' ')}/>
            <h2 className={styles.title}>{title}</h2>
            {children}
        </div>
    </div>
);

export default WrapperContent