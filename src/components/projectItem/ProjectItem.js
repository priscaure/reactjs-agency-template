import React from 'react'
import styles from './ProjectItem.module.scss'
import PropTypes from 'prop-types'
import Half from "../../commons/half/Half";

const leftContent = (title, description, date) => (
    <React.Fragment>
        <h3 className={styles.title}>{title}</h3>
        <div className={styles.description} dangerouslySetInnerHTML={{__html: description}}/>
        <div className={styles.date}>{date}</div>
    </React.Fragment>
);

const ProjectItem = ({id, title, description, date, image, reverse, className}) => (
    <React.Fragment>
        <Half
            halfClassName={[styles.projectItem, className].join(' ')}
            left={leftContent(title, description, date)}
            leftClassName={styles.left}
            right={<img className={styles.image} src={image} alt=""/>}
            rightClassName={styles.right}
            reverse={reverse}
            id={id}/>
    </React.Fragment>
);

ProjectItem.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
};

ProjectItem.defaultProps = {
    reverse: false
};

export default ProjectItem