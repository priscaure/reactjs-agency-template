import React from 'react'
import styles from './ProjectsList.module.scss'
import ProjectItem from "../projectItem/ProjectItem";

const ProjectsList = ({data, projectItemClassName}) => (
    <div className={styles.projectsList}>
        {
            data.map((item, index) => {
                return <ProjectItem
                    id={item.id}
                    key={index}
                    title={item.title}
                    description={item.description}
                    date={item.date}
                    image={item.image}
                    reverse={index % 2 === 1}
                    className={projectItemClassName}
                />
            })
        }
    </div>
);

export default ProjectsList