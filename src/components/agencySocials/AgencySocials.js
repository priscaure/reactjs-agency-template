import React from 'react'
import styles from './AgencySocials.module.scss'
import Socials from "../../commons/socials/Socials";

const AgencySocials = ({fill, networks}) => {
    return (
        <Socials
            fill={fill}
            className={styles.socials}
            networks={networks}/>
    );
};

export default AgencySocials
