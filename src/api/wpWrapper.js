export const wpPage = (page) => {
    return {
        left: page.title.rendered,
        right: page.content.rendered,
    }
};

export const wpHome = (page) => {
    return {
        header: {
            ...wpPage(page),
            linkUrl: page.acf.header_link_url,
            linkText: page.acf.header_link_text
        },
        quote: {
            content: page.acf.quote_text,
            linkUrl: page.acf.quote_link_url,
            linkText: page.acf.quote_link_text

        }
    }
};

const addImageProcess = (src) => {
    return new Promise((resolve, reject) => {
        let img = new Image()
        img.onload = () => resolve({height: img.height, width: img.width})
        img.onerror = reject
        img.src = src
    })
};

async function imageSize(imageUrl) {
    return await addImageProcess(imageUrl)
}

export const wpWhyUs = async (page) => {
    const images = await Promise.all(page.acf.gallery.map(async image => {
        let finalImage = await imageSize(image.guid);
        return {
            src: image.guid,
            width: finalImage.width,
            height: finalImage.height
        }
    }));
    return {
        header: {
            ...wpPage(page)
        },
        gallery: {
            title: page.acf.gallery_title,
            images
        },
        team: {
            title: page.acf.team_title
        }
    }
};

export const wpPartner = (article) => {
    return {
        image: article._embedded["wp:featuredmedia"]["0"].media_details.sizes.full.source_url,
        link: article.acf.link
    }
};

export const wpPartners = (data) => {
    return data.map(function (item) {
        return wpPartner(item);
    });
};

export const wpArticle = (article) => {
    const date = new Date(article.modified),
        locale = "fr-fr",
        month = date.toLocaleString(locale, {month: "long"});
    return {
        id: article.id,
        cover: article._embedded["wp:featuredmedia"]["0"].media_details.sizes.medium_large.source_url,
        date: date.getDate() + " " + month + " " + date.getFullYear(),
        title: article.title.rendered,
        content: article.content.rendered,
    }
};

export const wpArticles = (data) => {
    return data.map(function (item) {
        return wpArticle(item);
    });
};

export const wpProject = (project) => {
    return {
        id: project.id,
        title: project.title.rendered,
        description: project.content.rendered,
        date: project.acf.date,
        image: project._embedded["wp:featuredmedia"]["0"].media_details.sizes.medium_large.source_url,
        link: "/projects#" + project.id
    }
};

export const wpProjects = (data) => {
    return data.map(function (item) {
        return wpProject(item);
    });
};

export const wpTeamIndividual = (data) => {
    return {
        image: data._embedded["wp:featuredmedia"]["0"].media_details.sizes.full.source_url,
        name: data.title.rendered,
        job: data.content.rendered
    }
};

export const wpTeam = (data) => {
    return data.map(function (item) {
        return wpTeamIndividual(item);
    });
};
