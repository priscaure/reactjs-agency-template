import axios from 'axios';
import settings from '../settings.json'

export default axios.create({
    baseURL: settings.api.endpoint,
    timeout: 5000
});